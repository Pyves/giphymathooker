package GiphyMatHooker;
use Mojo::Base 'Mojolicious';
use Mojo::Upload;

sub startup {
    my $self = shift;

    $self->plugin('Config', {
        default => {
            apikey => 'dc6zaTOxFJmzC',
        }
    });

    $self->plugin('DebugDumperHelper');

    # Router
    my $r = $self->routes;

    # Normal route to controller
    $r->any('/' => sub {
        my $c = shift;
        # This is only to check that the request comes (apparently) from mattermost
        # I know that I should test the token, but it would prevent other users from
        # framateam to use this service
        # To enable token authent, just set a token in the configuration file
        if ((!defined($c->config('token')) &&
             defined($c->param('token')) &&
             defined($c->param('channel_id')) &&
             defined($c->param('channel_name')) &&
             defined($c->param('command')) &&
             defined($c->param('team_domain')) &&
             defined($c->param('team_id')) &&
             defined($c->param('user_id')) &&
             defined($c->param('user_name')) &&
             defined($c->param('response_url')))
             || (defined $c->config('token') && $c->param('token') eq $c->config('token'))
         ) {
            my $text = $c->param('text');

            my $url = Mojo::URL->new('https://api.giphy.com/v1/gifs/search');
            $url->query(
                q       => $text,
                api_key => $c->config('apikey'),
                limit   => 25
            );
            $c->ua->get($url => sub {
                my ($ua, $tx) = @_;
                if (my $res = $tx->success) {
                    if ($res->json->{pagination}->{total_count}) {
                        $c->render(
                            json => {
                                "response_type" => "in_channel",
                                "text"          => $res->json->{data}->[rand(scalar(@{$res->json->{data}}))]->{images}->{original}->{url}
                            }
                        );
                    } else {
                        $c->render(
                            json => {
                                "response_type" => "ephemeral",
                                "text"          => "Sorry, no results from giphy"
                            }
                        );
                    }
                }
            });

            $c->render_later;
        } else {
            $c->rendered(401);
        }
    });
    $r->any('/understood' => sub {
        shift->render(
            json => {
                "response_type" => "in_channel",
                "text"          => "https://media4.giphy.com/media/cRpEcJPIBEHPG/giphy.gif"
            }
        );
    });
    $r->any('/money' => sub {
        shift->render(
            json => {
                "response_type" => "in_channel",
                "text"          => "https://media0.giphy.com/media/2mXJvHKUYL9n2/giphy.gif"
            }
        );
    });
}

1;
